unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  ComCtrls;

type

  { TMainForm }

  TMainForm = class(TForm)
    CalculateButton: TButton;
    CompressionRadioGroup: TRadioGroup;
    FPSGroupBox: TGroupBox;
    BitratePerCameraLabel: TLabel;
    TotalBitrateLabel: TLabel;
    NumberOfCamerasGroupBox: TGroupBox;
    QualityRadioGroup: TRadioGroup;
    ResolutionRadioGroup: TRadioGroup;
    NumberOfCamerasTrackBar: TTrackBar;
    FPSTrackBar: TTrackBar;
    procedure CalculateButtonClick(Sender: TObject);
    procedure CompressionRadioGroupClick(Sender: TObject);
    procedure FPSTrackBarChange(Sender: TObject);
    procedure NumberOfCamerasTrackBarChange(Sender: TObject);
    procedure QualityRadioGroupClick(Sender: TObject);
    procedure ResolutionRadioGroupClick(Sender: TObject);
  private

  public

  end;

var
  MainForm: TMainForm;
  Resolution: String = 'QCIF';
  Compression: String = 'MJPEG';
  Quality: String = 'High';
  FPS: String = '1';
  NumberOfCameras: String = '1';

implementation

{$R *.lfm}

{ TMainForm }



procedure TMainForm.CalculateButtonClick(Sender: TObject);
var
  flags: String = '';
  strs: TStringList;
begin
  flags := '-r ' + Resolution + ' -c ' + Compression + ' -q ' + Quality + ' -f ' + FPS + ' -m ' + NumberOfCameras;
  SysUtils.ExecuteProcess('./cameras_bandwidth_calculator.exe', flags, []);
  strs := tStringList.Create;
  strs.LoadFromFile('finished_cameras_bandwidth_calculator.csv');
  MainForm.BitratePerCameraLabel.Caption := 'Bitrate per camera: ' + strs[1].Split(',')[0] + ' Mbps';
  MainForm.TotalBitrateLabel.Caption := 'Total bitrate: ' + strs[1].Split(',')[2] + ' Mbps';
end;

procedure TMainForm.CompressionRadioGroupClick(Sender: TObject);
begin
  Compression := MainForm.CompressionRadioGroup.Items[MainForm.CompressionRadioGroup.ItemIndex];
end;

procedure TMainForm.FPSTrackBarChange(Sender: TObject);
var
  NewFPS: String;
begin
  NewFPS := IntToStr(MainForm.FPSTrackBar.Position);
  MainForm.FPSGroupBox.Caption := 'FPS (1-30): ' + NewFPS;
  FPS := NewFPS;
end;

procedure TMainForm.NumberOfCamerasTrackBarChange(Sender: TObject);
var
  NewNumberOfCameras: String;
begin
  NewNumberOfCameras := IntToStr(MainForm.NumberOfCamerasTrackBar.Position);
  MainForm.NumberOfCamerasGroupBox.Caption := 'Number of cameras (1-15): ' + NewNumberOfCameras;
  NumberOfCameras := NewNumberOfCameras;
end;

procedure TMainForm.QualityRadioGroupClick(Sender: TObject);
begin
  Quality := MainForm.QualityRadioGroup.Items[MainForm.QualityRadioGroup.ItemIndex];
end;

procedure TMainForm.ResolutionRadioGroupClick(Sender: TObject);
begin
  Resolution := MainForm.ResolutionRadioGroup.Items[MainForm.ResolutionRadioGroup.ItemIndex];
end;

end.

